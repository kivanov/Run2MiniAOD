# How to use Analyzer for B-physics analysis via CMSSW 

### 1) Creating CMSSW area

To work with Run-2 UltraLegacy data, use the following code: 

```
mkdir Analyzers
cd Analyzers
cmsrel CMSSW_10_6_29
cd CMSSW_10_6_29/src
scram b

git clone --recursive ssh://git@gitlab.cern.ch:7999/kivanov/Run2MiniAOD.git
cd Run2MiniAOD/JPsiKKKPATRunII/
```
