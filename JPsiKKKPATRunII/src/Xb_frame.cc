/// -*- C++ -*-
//
// Package:    XbFrame/Xb_frame
// Class:      Xb_frame
//
/**\class Xb_frame Xb_frame.cc XbFrame/Xb_frame/plugins/Xb_frame.cc

 Description: Analyzer for b- or c-hadron reconstruction via RunII MiniAOD datasets

 Implementation:
     [Notes on implementation]
*/
//
// Original Author:  Sergey Polikarpov
//         Created:  Tue, 15 Aug 2017 01:04:12 GMT
//
// Refactored for MiniAOD:
//                   Kirill Ivanov
//         Created:  Sun, 18 Apr 2021

// system include files
#include <memory>
#include <ciso646> // and, or, not instead &&, ||, !

// MiniAOD
#include "DataFormats/PatCandidates/interface/PackedCandidate.h" // for miniAOD
#include "DataFormats/TrackReco/interface/Track.h" // for miniAOD

/// framework
#include "FWCore/Framework/interface/Frameworkfwd.h"
#include "FWCore/Framework/interface/EDAnalyzer.h"
#include "FWCore/Framework/interface/one/EDAnalyzer.h"
#include "FWCore/Framework/interface/Event.h"
#include "DataFormats/FWLite/interface/EventBase.h"
#include "FWCore/Framework/interface/MakerMacros.h"
#include "FWCore/ServiceRegistry/interface/Service.h"
#include "FWCore/ParameterSet/interface/ParameterSet.h"
#include "DataFormats/Common/interface/Handle.h"
/// triggers
#include "DataFormats/Common/interface/TriggerResults.h"
#include "FWCore/Common/interface/TriggerNames.h"
#include "DataFormats/HLTReco/interface/TriggerEvent.h"
#include "DataFormats/PatCandidates/interface/TriggerObjectStandAlone.h"
// #include "HLTrigger/HLTcore/interface/HLTConfigProvider.h"
/// tracks
#include "TrackingTools/TransientTrack/interface/TransientTrackBuilder.h"
#include "TrackingTools/Records/interface/TransientTrackRecord.h"
#include "TrackingTools/TransientTrack/interface/TransientTrack.h"
#include "TrackingTools/PatternTools/interface/ClosestApproachInRPhi.h"
#include "MagneticField/Engine/interface/MagneticField.h"
#include "DataFormats/Candidate/interface/ShallowCloneCandidate.h"
#include "DataFormats/PatCandidates/interface/GenericParticle.h"
#include "DataFormats/RecoCandidate/interface/RecoCandidate.h"
#include "DataFormats/RecoCandidate/interface/RecoChargedCandidateFwd.h"
#include "DataFormats/RecoCandidate/interface/RecoChargedCandidate.h"

/// muons
#include "DataFormats/MuonReco/interface/MuonChamberMatch.h"
#include "DataFormats/PatCandidates/interface/Muon.h"
#include "DataFormats/MuonReco/interface/Muon.h"
#include "DataFormats/MuonReco/interface/MuonFwd.h"

/// vertex fits
#include "RecoVertex/VertexTools/interface/VertexDistance.h"
#include "RecoVertex/VertexTools/interface/VertexDistance3D.h"
#include "RecoVertex/VertexTools/interface/VertexDistanceXY.h"
#include "DataFormats/Candidate/interface/VertexCompositeCandidate.h"
#include "DataFormats/V0Candidate/interface/V0Candidate.h"
#include "RecoVertex/KinematicFitPrimitives/interface/MultiTrackKinematicConstraint.h"
#include "RecoVertex/KinematicFit/interface/KinematicConstrainedVertexFitter.h"
#include "RecoVertex/KinematicFit/interface/TwoTrackMassKinematicConstraint.h"
#include "RecoVertex/KinematicFitPrimitives/interface/KinematicParticleFactoryFromTransientTrack.h"
#include "RecoVertex/KinematicFit/interface/KinematicConstrainedVertexFitter.h"
#include "RecoVertex/KinematicFit/interface/TwoTrackMassKinematicConstraint.h"
#include "RecoVertex/KinematicFit/interface/MassKinematicConstraint.h"
#include "RecoVertex/KinematicFit/interface/KinematicParticleVertexFitter.h"
#include "RecoVertex/KinematicFit/interface/KinematicParticleFitter.h"
#include "RecoVertex/AdaptiveVertexFit/interface/AdaptiveVertexFitter.h"
#include "RecoVertex/VertexPrimitives/interface/BasicSingleVertexState.h"
#include "RecoVertex/VertexPrimitives/interface/VertexState.h"
#include "DataFormats/BeamSpot/interface/BeamSpot.h"

//// gen ??
#include "SimDataFormats/GeneratorProducts/interface/HepMCProduct.h"
#include "DataFormats/Common/interface/RefToBase.h"
#include "DataFormats/HepMCCandidate/interface/GenParticle.h"
#include "DataFormats/Candidate/interface/CandMatchMap.h"
#include "SimDataFormats/GeneratorProducts/interface/GenEventInfoProduct.h"
#include "DataFormats/CLHEP/interface/Migration.h"

#include "CommonTools/UtilAlgos/interface/TFileService.h"
#include "DataFormats/Math/interface/Error.h"
#include "TFile.h"
#include "TTree.h"

#include <vector>
#include "CommonTools/Statistics/interface/ChiSquaredProbability.h"
#include "TLorentzVector.h"
#include <utility>
#include <string>
#include <map>
//
///////
///// UPDATED TO 2018 PDG PARTICLES !!
/// {{{
double PDG_MUON_MASS    =   0.1056583745;
double PDG_PION_MASS    =   0.13957061;
double PDG_PIOZ_MASS    =   0.1349770;
double PDG_KAON_MASS    =   0.493677;
double PDG_PROTON_MASS  =   0.9382720813;
double PDG_KSHORT_MASS  =   0.497611;
double PDG_KSHORT_DM    =   0.000013;
double PDG_KSHORT_TIME  =   0.8954 * 0.0000000001;
double PDG_KS_MASS      =   PDG_KSHORT_MASS;
double PDG_LAMBDA_MASS  =   1.115683 ;
double PDG_LAMBDA_DM    =   0.000006 ;
double PDG_LAMBDA_TIME  =   2.632 * 0.0000000001;
double PDG_SIGMA0_MASS  =   1.192642 ;
double PDG_XImunus_MASS =   1.32171 ;
double PDG_XImunus_DM   =   0.00007 ;
double PDG_XImunus_TIME =   1.639 * 0.0000000001;
double PDG_OMmunus_MASS =   1.67245 ;
double PDG_OMmunus_DM   =   0.00029 ;
double PDG_OMmunus_TIME =   0.821 * 0.0000000001;
double PDG_DPM_MASS     =   1.86965 ;
double PDG_DPM_DM       =   0.00005 ;
double PDG_DPM_TIME     =   1.040 * 0.000000000001 ;
double PDG_DZ_MASS      =   1.86483 ;
double PDG_DZ_DM        =   0.00005 ;
double PDG_DZ_TIME      =   0.4101 * 0.000000000001 ;
double PDG_DS_MASS      =   1.96834 ;
double PDG_DS_DM        =   0.00007 ;
double PDG_DS_TIME      =   0.504 * 0.000000000001 ;
double PDG_LAMCZ_MASS   =   2.28646 ;
double PDG_LAMCZ_DM     =   0.00031 ;
double PDG_LAMCZ_TIME   =   2.00 * 0.0000000000001;
double PDG_XICZ_MASS    =   2.47087 ;
double PDG_XICZ_DM      =   0.00031 ;
double PDG_XICZ_TIME    =   1.12 * 0.0000000000001;
double PDG_XICP_MASS    =   2.46787 ;
double PDG_XICP_DM      =   0.00030 ;
double PDG_XICP_TIME    =   4.42 * 0.0000000000001;
double PDG_KSTARZ_MASS  =   0.89555;
double PDG_KSTARZ_GAMMA =   0.0473 ;
double PDG_KSTARP_MASS  =   0.89176;
double PDG_KSTARP_GAMMA =   0.0503 ;
double PDG_PHI_MASS     =   1.019461;
double PDG_PHI_GAMMA    =   0.004249;
double PDG_JPSI_MASS    =   3.096900;
double PDG_PSI2S_MASS   =   3.686097;
double PDG_X3872_MASS   =   3.87165;
double PDG_BU_MASS      =   5.27932;
double PDG_BU_TIME      =   1.638 * 0.000000000001;
double PDG_B0_MASS      =   5.27963;
double PDG_B0_TIME      =   1.520 * 0.000000000001;
double PDG_BS_MASS      =   5.36689;
double PDG_BS_TIME      =   1.509 * 0.000000000001;
double PDG_BC_MASS      =   6.2749;
double PDG_BC_TIME      =   0.507 * 0.000000000001;
double PDG_LB_MASS      =   5.61960;
double PDG_LB_TIME      =   1.470 * 0.000000000001;
double PDG_XIBZ_MASS    =   5.7919;
double PDG_XIBZ_TIME    =   1.479 * 0.000000000001;
double PDG_XIBM_MASS    =   5.7970;
double PDG_XIBM_TIME    =   1.571 * 0.000000000001;
double PDG_OMBM_MASS    =   6.0461;
double PDG_OMBM_TIME    =   1.64 * 0.000000000001;
double PDG_C            =   29979245800.; // in cm/c
//
/// }}}

ParticleMass PM_PDG_MUON_MASS = PDG_MUON_MASS;
ParticleMass PM_PDG_JPSI_MASS = PDG_JPSI_MASS;
//ParticleMass PM_PDG_PHI_MASS  = PDG_PHI_MASS;
ParticleMass PM_PDG_KAON_MASS = PDG_KAON_MASS;
ParticleMass PM_PDG_PION_MASS = PDG_PION_MASS;
//ParticleMass PM_PDG_PROTON_MASS = PDG_PROTON_MASS;
ParticleMass PM_PDG_PSI2S_MASS = PDG_PSI2S_MASS;
ParticleMass PM_PDG_X3872_MASS = PDG_X3872_MASS;

//
//
// class declaration
//

Int_t N_written_events = 0;

//

class Xb_frame : public edm::one::EDAnalyzer<edm::one::SharedResources>  {
public:
    explicit Xb_frame(const edm::ParameterSet&);
    ~Xb_frame();

    bool IsTheSame(const pat::GenericParticle& tk, const pat::Muon& mu);

    static void fillDescriptions(edm::ConfigurationDescriptions& descriptions);


private:
    virtual void beginJob() override;
    virtual void analyze(const edm::Event&, const edm::EventSetup&) override;
    virtual void endJob() override;

    // ----------member data ---------------------------
// HLTConfigProvider hltConfig_;

    // EDGetToken for MiniAOD:

    edm::EDGetTokenT<edm::View<pat::Muon>>                        muons_;
    edm::EDGetTokenT<edm::View<pat::PackedCandidate>>             tracks_;
    edm::EDGetTokenT<reco::VertexCollection>                      vtxSample_;
    edm::EDGetTokenT<reco::BeamSpot>                              thebeamspot_;
    edm::EDGetTokenT<edm::TriggerResults>                         hlTriggerResults_;
    edm::EDGetTokenT<reco::VertexCompositePtrCandidateCollection> tok_v0_;

    std::vector<float> *B_J_Prob;
    std::vector<float> *B_J_mass;
    std::vector<float> *B_J_px          , *B_J_py         , *B_J_pz;
    std::vector<float> *B_J_DecayVtxX   , *B_J_DecayVtxY  , *B_J_DecayVtxZ;
    std::vector<float> *B_J_DecayVtxXE  , *B_J_DecayVtxYE , *B_J_DecayVtxZE;
    std::vector<float> *B_mu_px1_cjp    , *B_mu_py1_cjp   , *B_mu_pz1_cjp;
    std::vector<float> *B_mu_px1        , *B_mu_py1       , *B_mu_pz1;
    std::vector<float> *B_mu_ips1       , *B_mu_hit1      , *B_mu_pix1;
    std::vector<float> *B_mu_px2_cjp    , *B_mu_py2_cjp   , *B_mu_pz2_cjp;
    std::vector<float> *B_mu_px2        , *B_mu_py2       , *B_mu_pz2;
    std::vector<float> *B_mu_ips2       , *B_mu_hit2      , *B_mu_pix2;

    std::vector<float> *B_mass     , *B_mass_c0   , *B_Prob_c0, *B_Prob;
    std::vector<int>   *B_ka_charg;
    std::vector<float> *B_px       , *B_py        , *B_pz;
    std::vector<float> *B_ka_px    , *B_ka_py     , *B_ka_pz;
    std::vector<float> *B_ka_px_CV , *B_ka_py_CV  , *B_ka_pz_CV;
    std::vector<float> *B_DecayVtxX, *B_DecayVtxY , *B_DecayVtxZ;
    std::vector<float> *B_DecayVtxXE, *B_DecayVtxYE, *B_DecayVtxZE;
    std::vector<int>   *B_ka_hit, *B_ka_pix;
    std::vector<float> *B_ka_ips;
    std::vector<float> *B_pvdistsignif2_Cjp, *B_pvcos2_Cjp;

    std::vector<float> *PV_becos_XX , *PV_becos_YY  , *PV_becos_ZZ;
    std::vector<float> *PV_becos_EX , *PV_becos_EY  , *PV_becos_EZ;
    std::vector<float> *PV_becos_CL;
    std::vector<int>   *PV_becos_dN;

    std::vector<short>  *trig0;
    std::vector<short>  *trig1;
    std::vector<short>  *trig2;
    std::vector<short>  *trig3;
    std::vector<short>  *trig4;
    std::vector<short>  *trig5;
    std::vector<short>  *trig6;
    std::vector<short>  *trig7;
    std::vector<short>  *trig8;
    std::vector<short>  *trig9;
    std::vector<short>  *trig10;
    std::vector<short>  *trig11;
    std::vector<short>  *trig12;
    std::vector<short>  *trig13;

    std::vector<short>  *trig0_fire;
    std::vector<short>  *trig1_fire;
    std::vector<short>  *trig2_fire;
    std::vector<short>  *trig3_fire;
    std::vector<short>  *trig4_fire;
    std::vector<short>  *trig5_fire;
    std::vector<short>  *trig6_fire;
    std::vector<short>  *trig7_fire;
    std::vector<short>  *trig8_fire;
    std::vector<short>  *trig9_fire;
    std::vector<short>  *trig10_fire;
    std::vector<short>  *trig11_fire;
    std::vector<short>  *trig12_fire;
    std::vector<short>  *trig13_fire;

    Int_t       nCand;

    Int_t       run;
    Int_t       event;
    Float_t     lumi;

    Int_t       numPV;
    Int_t       numTrack;
    Int_t       numMuon;
    Int_t       numV0;

    TTree *wwtree;
    TFile *f;
    std::string fileName;

};

//
Xb_frame::Xb_frame(const edm::ParameterSet& iConfig) :
        muons_(consumes<edm::View<pat::Muon>>(iConfig.getParameter<edm::InputTag>("Muons"))),
        tracks_(consumes<edm::View<pat::PackedCandidate>>(iConfig.getParameter<edm::InputTag>("Tracks"))),
        vtxSample_(consumes<reco::VertexCollection>(iConfig.getParameter<edm::InputTag>("VtxSample"))),
        thebeamspot_(consumes<reco::BeamSpot>(iConfig.getParameter<edm::InputTag>("beamSpotTag"))),
        hlTriggerResults_(consumes<edm::TriggerResults>(iConfig.getParameter<edm::InputTag>("TriggerResults"))),
//        tok_v0_(consumes<reco::VertexCompositePtrCandidateCollection>(iConfig.getParameter<edm::InputTag>("secundaryVerticesPtr"))),
//        genTag_(           iConfig.getUntrackedParameter<edm::InputTag>("genParticlesTag")),
//        genToken_(         consumes<reco::GenParticleCollection>(genTag_)),

        B_J_Prob(0),        B_J_mass(0),
        B_J_px(0),          B_J_py(0),          B_J_pz(0),
        B_J_DecayVtxX(0),   B_J_DecayVtxY(0),   B_J_DecayVtxZ(0),
        B_J_DecayVtxXE(0),  B_J_DecayVtxYE(0),  B_J_DecayVtxZE(0),

        B_mu_px1_cjp(0),    B_mu_py1_cjp(0),    B_mu_pz1_cjp(0),
        B_mu_px1(0),        B_mu_py1(0),        B_mu_pz1(0),
        B_mu_ips1(0),       B_mu_hit1(0),       B_mu_pix1(0),

        B_mu_px2_cjp(0),    B_mu_py2_cjp(0),    B_mu_pz2_cjp(0),
        B_mu_px2(0),        B_mu_py2(0),        B_mu_pz2(0),
        B_mu_ips2(0),       B_mu_hit2(0),       B_mu_pix2(0),

        B_mass(0)      , B_mass_c0(0) , B_Prob_c0(0), B_Prob(0), B_ka_charg(0),
        B_px(0)        , B_py(0)      , B_pz(0),
        B_ka_px(0)     , B_ka_py(0)   , B_ka_pz(0),
        B_ka_px_CV(0)  , B_ka_py_CV(0), B_ka_pz_CV(0),
        B_DecayVtxX(0)      , B_DecayVtxY(0)    , B_DecayVtxZ(0),
        B_DecayVtxXE(0)     , B_DecayVtxYE(0)   , B_DecayVtxZE(0),
        B_ka_hit(0)    , B_ka_pix(0)  , B_ka_ips(0),
        B_pvdistsignif2_Cjp(0), B_pvcos2_Cjp(0),

        PV_becos_XX(0)  , PV_becos_YY(0), PV_becos_ZZ(0),
        PV_becos_EX(0)  , PV_becos_EY(0), PV_becos_EZ(0),
        PV_becos_CL(0)  , PV_becos_dN(0),

        trig0(0),    trig1(0),    trig2(0),
        trig3(0),    trig4(0),    trig5(0),
        trig6(0),    trig7(0),    trig8(0),
        trig9(0),    trig10(0),   trig11(0),
        trig12(0),   trig13(0),

        trig0_fire(0),    trig1_fire(0),    trig2_fire(0),
        trig3_fire(0),    trig4_fire(0),    trig5_fire(0),
        trig6_fire(0),    trig7_fire(0),    trig8_fire(0),
        trig9_fire(0),    trig10_fire(0),   trig11_fire(0),
        trig12_fire(0),   trig13_fire(0),

//
        nCand(0),

        run(0),
        event(0),
        lumi(0),

        numPV(0),
        numTrack(0),
        numMuon(0),
        numV0(0)
{
    fileName = iConfig.getUntrackedParameter<std::string>("fileName","BFinder.root");
    //now do what ever initialization is needed
    usesResource("TFileService");

    // AOD:
//    tok_v0_     = consumes<reco::VertexCompositeCandidateCollection>(   edm::InputTag("generalV0Candidates:Lambda"));

    // MiniAOD:
    //    tok_v0_         =   consumes<reco::VertexCompositePtrCandidateCollection>(edm::InputTag("slimmedKshortVertices")); /// miAOD
    tok_v0_         =   consumes<reco::VertexCompositePtrCandidateCollection>(edm::InputTag("slimmedLambdaVertices")); /// miAOD

}


Xb_frame::~Xb_frame()
{

    // do anything here that needs to be done at desctruction time
    // (e.g. close files, deallocate resources etc.)

}


//
// member functions
//

// ------------ method called for each event  ------------
void
Xb_frame::analyze(const edm::Event& iEvent, const edm::EventSetup& iSetup)
{
    using namespace edm;
    using namespace reco;
    using namespace std;
    using reco::MuonCollection;

    run   = iEvent.id().run();
    event = iEvent.id().event();

    lumi = 1;
    lumi = iEvent.luminosityBlock();

    // declare new track builder for new Transient track collection for miniAOD ???
    ESHandle<TransientTrackBuilder> theB;
    iSetup.get<TransientTrackRecord>().get("TransientTrackBuilder",theB);

// Get HLT results HLT   HLT  HLT   HLT  HLT   HLT
    edm::Handle<edm::TriggerResults> triggerResults_handle;
    iEvent.getByToken(hlTriggerResults_, triggerResults_handle);

//// PV PV  PV  PV  PV  PV  PV  PV  PV  PV  PV  PV  PV
    Handle < VertexCollection > recVtxs;
    iEvent.getByToken(vtxSample_, recVtxs);
///
    Vertex thePrimaryV;
    thePrimaryV = Vertex(*(recVtxs->begin()));
    const reco::VertexCollection & vertices = *recVtxs.product();

///  MUONS + TRACKS    MUONS + TRACKS    MUONS + TRACKS
    edm::Handle< View<pat::Muon> > thePATMuonHandle;
    iEvent.getByToken(muons_, thePATMuonHandle);
    edm::Handle< View< pat::PackedCandidate > > thePATTrackHandle;
    iEvent.getByToken(tracks_, thePATTrackHandle);

////  V0  V0  V0  V0  V0  V0  V0  V0  V0  V0  V0  V0
    Handle<reco::VertexCompositePtrCandidateCollection> v0Coll; // miAOD
    iEvent.getByToken(tok_v0_, v0Coll);

    numPV       = vertices.size();
    numTrack    = thePATTrackHandle->size();
    numMuon     = thePATMuonHandle->size();
    numV0       = v0Coll->size();

//    if (numV0   < 1 ) return;
    if (numTrack< 1 ) return;

    unsigned int NTRIGGERS = 14;

#pragma clang diagnostic push
#pragma ide diagnostic ignored "UnusedValue"
    std::vector<string> TriggerPaths = {
            "HLT_Dimuon25_Jpsi_v*", "HLT_Dimuon20_Jpsi_Barrel_Seagulls_v*", // 0, 1: inclusive dimuon jpsi
            "HLT_DoubleMu4_JpsiTrk_Displaced_v*", "HLT_DoubleMu4_JpsiTrkTrk_Displaced_v*", // 2, 3: displaced jpistrk or jpsi trktrk
            "HLT_DoubleMu4_3_Jpsi_Displaced_v*", "HLT_DoubleMu4_3_Jpsi_v*", // 4, 5: prescaled, 4 for 2017, 5 for 2018
            "HLT_DoubleMu4_Jpsi_Displaced_v*",  // 6: prescaled,
            "HLT_Dimuon18_PsiPrime_v*", "HLT_Dimuon10_PsiPrime_Barrel_Seagulls_v*", // 7, 8: inclusive dimuon psi2s
            "HLT_DoubleMu4_PsiPrimeTrk_Displaced_v*", // 9: displaced psi2s trk
            "HLT_Dimuon0_Jpsi3p5_Muon2_v*", // 10: triple-mu (jpsi + muon)
            "HLT_DoubleMu2_Jpsi_DoubleTkMu0_Phi_v*", // 11: jpsi + 2 trkmu (phi->mumu)
            "HLT_DoubleMu2_Jpsi_DoubleTrk1_Phi_v*", "HLT_DoubleMu2_Jpsi_DoubleTrk1_Phi1p05_v*" // 12, 13: jpsi+2 trk(phi->KK), 12 for 2017, 13 for 2018
    };
#pragma clang diagnostic pop

    std::vector<int> TriggerResults;


    /////////////////////////
    /////////////////////////
    //  TRIGGER FIRED //
//  {{{{

    unsigned short int TriggersFired[NTRIGGERS] = {};
    std::string strfired;
    int trig = 0; // bit-based variable for trigger description

    if (triggerResults_handle.isValid())
    {
        const edm::TriggerNames & TheTriggerNames = iEvent.triggerNames(*triggerResults_handle);
        for (unsigned int i = 0; i < NTRIGGERS; i++)
        {
            for (int version = 1; version < 30; version++)
            {
                std::stringstream ss; // full trigger name
                ss << TriggerPaths[i].substr(0, TriggerPaths[i].size()-3) << "_v" << version;
                //
                unsigned int bit = TheTriggerNames.triggerIndex(edm::InputTag(ss.str()).label());
                if (bit < triggerResults_handle->size() and triggerResults_handle->accept(bit) and not triggerResults_handle->error(bit))
                {
                    trig += (1<<i);
                    TriggersFired[i] = 1;
                    strfired.append( ss.str());
                    strfired.append( " " );
                    //break;
                }
            }
        }
    }
    else
    {
        std::cout << " No trigger Results in event :( " << run << "," << event << std::endl;
    }
//
//  }}}}


    nCand = 0;

    float PM_sigma = 1.e-7;
    KinematicParticleFactoryFromTransientTrack pFactory;

// get Phi -> mu mu
    for (View<pat::Muon >::const_iterator iMuonP = thePATMuonHandle->begin(); iMuonP != thePATMuonHandle->end(); ++iMuonP)
    {
        if (iMuonP->charge() < 0.5) continue; // select mu-
        if (not iMuonP->isSoftMuon(thePrimaryV)) continue;

        // next check for mu+
        for (View <pat::Muon >::const_iterator iMuonM = thePATMuonHandle->begin(); iMuonM != thePATMuonHandle->end(); ++iMuonM)
        {
            if (iMuonM->charge() > -0.5) continue; // select mu+

            TrackRef muTrackM = iMuonM->track();    // first track is accepted as plus charged
            if (muTrackM.isNull()) continue;
            if (not iMuonM->isSoftMuon(thePrimaryV)) continue;

            TrackRef glbTrackM, glbTrackP;
            glbTrackP = iMuonP->track();
            glbTrackM = iMuonM->track();
            if (glbTrackP.isNull() or glbTrackM.isNull()) continue;
            //
            reco::TransientTrack muonPTT((*theB).build(glbTrackP));
            reco::TransientTrack muonMTT((*theB).build(glbTrackM));
            //
            if (not muonPTT.isValid()) continue;
            if (not muonMTT.isValid()) continue;
            if (iMuonP->pt() < 3.0) continue;
            if (iMuonM->pt() < 3.0) continue;
            if (fabs(iMuonP->eta()) > 2.4 or fabs(iMuonM->eta()) > 2.4)  continue;
            if (not (glbTrackM->quality(reco::TrackBase::highPurity)) )  continue; //quality
            if (not (glbTrackP->quality(reco::TrackBase::highPurity)) )  continue; //quality

            // Get The Phi information from the tracks
            TLorentzVector p4mup_0c,p4mum_0c;
            p4mup_0c.SetPtEtaPhiM(iMuonP->pt(), iMuonP->eta(), iMuonP->phi(), PDG_MUON_MASS);
            p4mum_0c.SetPtEtaPhiM(iMuonM->pt(), iMuonM->eta(), iMuonM->phi(), PDG_MUON_MASS);
            //
            float muon_sigma = PDG_MUON_MASS * 1.e-6;
            float chi = 0.;
            float ndf = 0.;
            //
            vector <RefCountedKinematicParticle> muonParticles;
            muonParticles.push_back(pFactory.particle(muonPTT, PM_PDG_MUON_MASS, chi, ndf, muon_sigma));
            muonParticles.push_back(pFactory.particle(muonMTT, PM_PDG_MUON_MASS, chi, ndf, muon_sigma));
            KinematicParticleVertexFitter fitter;
            RefCountedKinematicTree phiVFT_noC;
            int fitgood = 1;
            try
            {
                phiVFT_noC = fitter.fit(muonParticles);   // fit to the muon pair
            }
            catch (VertexException eee)
            {
                fitgood = 0;
            }
            if (fitgood == 0) continue;

            if (not phiVFT_noC->isValid()) continue;
            //
            phiVFT_noC->movePointerToTheTop();
            RefCountedKinematicParticle MUMUparticle    = phiVFT_noC->currentParticle();
            RefCountedKinematicVertex   MUMUvtx         = phiVFT_noC->currentDecayVertex();
            //
            double MUMU_mass_c0 = MUMUparticle->currentState().mass();

            if ( MUMU_mass_c0 < 2.9) continue;
            if ( MUMU_mass_c0 > 4.0) continue;

            // Define if we have J/psi --> mu+mu- or psi(2S) --> mu+mu-
            int psi2 = 0;
//            int X3872 = 0;
            if (MUMU_mass_c0 > 3.45)  {psi2=1;}
            ParticleMass    PM_MYPSI_MASS = 3.1;
            if (psi2 == 0)    {PM_MYPSI_MASS = PM_PDG_JPSI_MASS;}
            if (psi2 == 1)    {PM_MYPSI_MASS = PM_PDG_PSI2S_MASS;}

            //
            double MUMU_vtxprob = TMath::Prob(MUMUvtx->chiSquared(), MUMUvtx->degreesOfFreedom());
            if(MUMU_vtxprob < 0.01) continue;
            //
            phiVFT_noC->movePointerToTheFirstChild();
            RefCountedKinematicParticle MUP_cMUMU       = phiVFT_noC->currentParticle();
            phiVFT_noC->movePointerToTheNextChild();
            RefCountedKinematicParticle MUM_cMUMU       = phiVFT_noC->currentParticle();
            // GlobalVector MUP_cMUMU_p    = MUP_cMUMU->currentState().kinematicParameters().momentum();
            // GlobalVector MUM_cMUMU_p    = MUM_cMUMU->currentState().kinematicParameters().momentum();
            //
//
            // ********************* muon-trigger-machint ****************
            // {{{{{

            const pat::Muon* muonP = &(*iMuonP);
            const pat::Muon* muonM = &(*iMuonM);

            for (unsigned int i = 0; i < NTRIGGERS; i++) {
                int trigger_tmp = 0;

                if (muonP->triggerObjectMatchByPath(TriggerPaths[i])!=nullptr && muonM->triggerObjectMatchByPath(TriggerPaths[i])!=nullptr) trigger_tmp = 1;
                TriggerResults.push_back(trigger_tmp);
            }
            // }}}}}}
            ////
            ////

            for (View<pat::PackedCandidate>::const_iterator iTrack1 = thePATTrackHandle->begin(); iTrack1 != thePATTrackHandle->end(); ++iTrack1 )
            {

                // To remove Exception Message:
                // Trying to access covariance matrix for a PackedCandidate for which it's not available.
                // Check hasTrackDetails() before!
                if (not (iTrack1->hasTrackDetails())) continue;

                auto patTrack1 = iTrack1->bestTrack();

                if (IsTheSame(*iTrack1,*iMuonP) || IsTheSame(*iTrack1,*iMuonM)) continue;
                if (not (iTrack1->trackHighPurity())) continue;

                if (iTrack1->pt() < 1.2) continue;  // 0.3
                if (fabs(iTrack1->eta()) > 2.4 ) continue; // 3.0
//                if (iTrack1->charge() * protTT.charge() > -0.5) continue;
                if (iTrack1->charge() == 0) continue;


                reco::TransientTrack kaon1TT((*theB).build(iTrack1->pseudoTrack()));
                if (not kaon1TT.isValid()) continue;
                if (muonPTT == kaon1TT) continue;
                if (muonMTT == kaon1TT) continue;

                TLorentzVector p4ka1;
                p4ka1.SetPtEtaPhiM(iTrack1->pt(),iTrack1->eta(),iTrack1->phi(), PDG_KAON_MASS);
                // std::cout << run << ":" << event << "--" << "found pi1 pt " <<  p4pi1.Pt() << std::endl;

                if ( (p4ka1 + p4mum_0c + p4mup_0c).M() > 6) {  // B_u mass = 5.27, skip very large mass > 7
//                    cout << "Ckipped candiate with M = " << (p4ka1 + p4mum_0c + p4mup_0c).M() << endl;
                    continue;
                }

                ///////
                ///////////////////////////////  B VERTEX FIT /////////////////////////////////////////
                ///////////////////////////////  B VERTEX FIT /////////////////////////////////////////
                ///////

                std::vector <RefCountedKinematicParticle> B_candidate_init;
                B_candidate_init.push_back(pFactory.particle(muonPTT, PM_PDG_MUON_MASS, chi,ndf, muon_sigma));
                B_candidate_init.push_back(pFactory.particle(muonMTT, PM_PDG_MUON_MASS, chi,ndf, muon_sigma));
                B_candidate_init.push_back(pFactory.particle(kaon1TT, PM_PDG_KAON_MASS, chi,ndf, PM_sigma));
                RefCountedKinematicTree BTree, vertexFitTree;

                std::vector<RefCountedKinematicParticle> B_candidate = B_candidate_init;
                KinematicParticleVertexFitter cFitter; //KinematicParticleVertexFitter
                fitgood = 1;
                try {
                    BTree = cFitter.fit(B_candidate_init);
                }
                catch (VertexException eee) {
                    fitgood = 0;
                }
                if (fitgood == 0) continue;

                if (not BTree->isValid()) continue;

                BTree->movePointerToTheTop();
                RefCountedKinematicParticle B_vFit = BTree->currentParticle();
                RefCountedKinematicVertex B_vtx = BTree->currentDecayVertex();
                //
                double B_mass_c0_tmp = B_vFit->currentState().mass();
                double B_Prob_c0_tmp = TMath::Prob(B_vtx->chiSquared(), (int) B_vtx->degreesOfFreedom());
//                std::cout << "B_Prob_c0_tmp = " <<  B_Prob_c0_tmp << std::endl;
//                if(B_Prob_c0_tmp < 0.01) continue;

                //Jpsi mass constraint
                //Jpsi mass constraint

                B_candidate = B_candidate_init;
                MultiTrackKinematicConstraint *ConstraintPhiMass = new TwoTrackMassKinematicConstraint(PM_MYPSI_MASS);
                KinematicConstrainedVertexFitter kcvFitter; //KinematicParticleVertexFitter
                vertexFitTree = kcvFitter.fit(B_candidate, ConstraintPhiMass);
                if (not vertexFitTree->isValid()) continue;

                vertexFitTree->movePointerToTheTop();
                RefCountedKinematicParticle bCandMC         = vertexFitTree->currentParticle();
                RefCountedKinematicVertex   bDecayVertexMC  = vertexFitTree->currentDecayVertex();
                if (not bDecayVertexMC->vertexIsValid())  continue;

                double B_mass_cjp_tmp = bCandMC->currentState().mass();

                // Bu mass = 5.27
                if (B_mass_cjp_tmp < 4.9) continue; //
                if (B_mass_cjp_tmp > 5.7) continue; //

                if(bDecayVertexMC->chiSquared() < 0) continue;
                double B_Prob_tmp   = TMath::Prob(bDecayVertexMC->chiSquared(), (int) bDecayVertexMC->degreesOfFreedom());
//                std::cout << "B_Prob_tmp = " <<  B_Prob_tmp << std::endl;
                if(B_Prob_tmp < 0.01) continue;

                std::cout << "B mass constrained = " <<  B_mass_cjp_tmp << std::endl;

                // get children from final C fit
                vertexFitTree->movePointerToTheFirstChild();
                RefCountedKinematicParticle mu1CandMC    = vertexFitTree->currentParticle();
                vertexFitTree->movePointerToTheNextChild();
                RefCountedKinematicParticle mu2CandMC    = vertexFitTree->currentParticle();
                vertexFitTree->movePointerToTheNextChild();
                RefCountedKinematicParticle kaCandMC     = vertexFitTree->currentParticle();

                //
                GlobalVector kaCandMC_p  = kaCandMC->currentState().kinematicParameters().momentum();
                GlobalVector mu1CandMC_p = mu1CandMC->currentState().kinematicParameters().momentum();
                GlobalVector mu2CandMC_p = mu2CandMC->currentState().kinematicParameters().momentum();

                //
                //
                TLorentzVector p4_b;
                p4_b.SetXYZM(B_vFit->currentState().globalMomentum().x(),
                             B_vFit->currentState().globalMomentum().y(),
                             B_vFit->currentState().globalMomentum().z(), B_mass_cjp_tmp);

                reco::Vertex bestVtxBSIP;  /// using B fit and B vtx HERE !!
                // {{{ GET THE BEST PV BY CHOSING THE BEST POINTING ANGLE AND REMOVE B TRACKS FROM ITS FIT
                // ********************* todos los vertices primarios con constrain del Beam-Spot y escogemos el de mejor pointing angle ****************

                reco::Vertex vtxBSrf;

                Double_t pVtxBSIPX_temp = -10000.0;
                Double_t pVtxBSIPY_temp = -10000.0;
                Double_t pVtxBSIPZ_temp = -10000.0;
                Double_t pVtxBSIPXE_temp = -10000.0;
                Double_t pVtxBSIPYE_temp = -10000.0;
                Double_t pVtxBSIPZE_temp = -10000.0;
                Double_t pVtxBSIPCL_temp = -10000.0;
                Double_t pVtxBSIPdN_temp = 0;
                Double_t lip = -100000.0;

                for (size_t i = 0; i < recVtxs->size(); ++i)
                {
                    Double_t ptsum_ = 0;
                    const Vertex &vtxBS = (*recVtxs)[i];
                    vector <reco::TransientTrack> vertexTracks;
                    for (std::vector<TrackBaseRef>::const_iterator iTrack = vtxBS.tracks_begin(); iTrack != vtxBS.tracks_end(); ++iTrack)
                    {
                        TrackRef trackRef = iTrack->castTo<TrackRef>();
                        // the  tracks in the B cand are  patTrack1 patTrack2 glbTrackP glbTrackM
                        if (not ((glbTrackM == trackRef       ) or
                                 (glbTrackP == trackRef       ) or
                                 (patTrack1 == trackRef.get() ) ) )
                        {
                            TransientTrack tt = theB->build(trackRef);
                            vertexTracks.push_back(tt);
                            ptsum_ += trackRef->pt();
                        } //else { std::cout << "found track match with primary" << endl;}
                    }

                    // if no tracks in primary or no reco track included in primary then don't do anything
                    vtxBSrf = vtxBS;
                    GlobalPoint PVRfP = GlobalPoint(vtxBS.x(), vtxBS.y(), vtxBS.z());
                    if (vertexTracks.size() > 0 && (vtxBS.tracksSize() != vertexTracks.size())) {
                        AdaptiveVertexFitter theFitter;
                        TransientVertex v = theFitter.vertex(vertexTracks, PVRfP);
                        if (v.isValid()) {
                            vtxBSrf = reco::Vertex(v);
                        }
                    }
                    Double_t dx = (*B_vtx).position().x() - vtxBSrf.x();
                    Double_t dy = (*B_vtx).position().y() - vtxBSrf.y();
                    Double_t dz = (*B_vtx).position().z() - vtxBSrf.z();
                    Double_t cosAlphaXYb = (B_vFit->currentState().globalMomentum().x() * dx +
                                            B_vFit->currentState().globalMomentum().y() * dy +
                                            B_vFit->currentState().globalMomentum().z() * dz) /
                                           (sqrt(dx * dx + dy * dy + dz * dz) *
                                            B_vFit->currentState().globalMomentum().mag());
                    if (cosAlphaXYb > lip) {
                        lip = cosAlphaXYb;
                        pVtxBSIPX_temp = vtxBSrf.x();
                        pVtxBSIPY_temp = vtxBSrf.y();
                        pVtxBSIPZ_temp = vtxBSrf.z();
                        pVtxBSIPXE_temp = vtxBSrf.covariance(0, 0);
                        pVtxBSIPYE_temp = vtxBSrf.covariance(1, 1);
                        pVtxBSIPZE_temp = vtxBSrf.covariance(2, 2);
                        pVtxBSIPCL_temp = (TMath::Prob(vtxBSrf.chi2(), (int) vtxBSrf.ndof()));
                        pVtxBSIPdN_temp = vtxBS.tracksSize() - vertexTracks.size();
                        bestVtxBSIP = vtxBSrf;
                    }
                }
//
////
                // B detach cuts !!
                double XB_pvdistsignif2_Cjp_tmp = 0.0;
                double deltaX1 = 0.0;
                double deltaY1 = 0.0;

                deltaX1 = (*B_vtx).position().x() - bestVtxBSIP.x();
                deltaY1 = (*B_vtx).position().y() - bestVtxBSIP.y();
                XB_pvdistsignif2_Cjp_tmp = sqrt(( deltaX1*deltaX1 )/ ( (*B_vtx).error().cxx() + bestVtxBSIP.covariance(0, 0)) + (deltaY1*deltaY1) / ((*B_vtx).error().cyy() + bestVtxBSIP.covariance(1, 1)));
                if (XB_pvdistsignif2_Cjp_tmp < 0.5) continue;

                double XB_pvcos2_Cjp_tmp = (B_vFit->currentState().globalMomentum().x() * deltaX1 + B_vFit->currentState().globalMomentum().y() * deltaY1) / ( sqrt(deltaX1*deltaX1 + deltaY1*deltaY1) * p4_b.Pt() );
                if (XB_pvcos2_Cjp_tmp < 0.9) continue;

                // ips (ka) > 1
                if ( fabs(iTrack1->dxy(bestVtxBSIP.position())) / (0.000001 + fabs(iTrack1->dxyError())) < 1 ) continue;

                ////
                //////////////////   SAVE ////
                //////////////////   SAVE ////
                //////////////////   SAVE
                B_J_Prob        ->push_back( MUMU_vtxprob                                         );
                B_J_mass        ->push_back( MUMU_mass_c0                                         );
                B_J_px          ->push_back( MUMUparticle->currentState().globalMomentum().x()    );
                B_J_py          ->push_back( MUMUparticle->currentState().globalMomentum().y()    );
                B_J_pz          ->push_back( MUMUparticle->currentState().globalMomentum().z()    );
                B_J_DecayVtxX   ->push_back( MUMUvtx->position().x()                              );
                B_J_DecayVtxY   ->push_back( MUMUvtx->position().y()                              );
                B_J_DecayVtxZ   ->push_back( MUMUvtx->position().z()                              );
                B_J_DecayVtxXE  ->push_back( MUMUvtx->error().cxx()                               );
                B_J_DecayVtxYE  ->push_back( MUMUvtx->error().cyy()                               );
                B_J_DecayVtxZE  ->push_back( MUMUvtx->error().czz()                               );

                B_mu_px1_cjp    ->push_back(mu1CandMC_p.x()                                         );
                B_mu_py1_cjp    ->push_back(mu1CandMC_p.y()                                         );
                B_mu_pz1_cjp    ->push_back(mu1CandMC_p.z()                                         );
                B_mu_px1        ->push_back(p4mup_0c.Px()                                           );
                B_mu_py1        ->push_back(p4mup_0c.Py()                                           );
                B_mu_pz1        ->push_back(p4mup_0c.Pz()                                           );
                B_mu_ips1       ->push_back(fabs(glbTrackP->dxy(bestVtxBSIP.position())) /
                                            (0.000001 + fabs(glbTrackP->dxyError()))                        );
                B_mu_hit1       ->push_back(glbTrackP->numberOfValidHits()                          );
                B_mu_pix1       ->push_back(glbTrackP->hitPattern().numberOfValidPixelHits()        );

                B_mu_px2_cjp    ->push_back(mu2CandMC_p.x()                                         );
                B_mu_py2_cjp    ->push_back(mu2CandMC_p.y()                                         );
                B_mu_pz2_cjp    ->push_back(mu2CandMC_p.z()                                         );
                B_mu_px2        ->push_back(p4mum_0c.Px()                                           );
                B_mu_py2        ->push_back(p4mum_0c.Py()                                           );
                B_mu_pz2        ->push_back(p4mum_0c.Pz()                                           );
                B_mu_ips2       ->push_back(fabs(glbTrackM->dxy(bestVtxBSIP.position())) /
                                            (0.000001 + fabs(glbTrackM->dxyError()))                );
                B_mu_hit2       ->push_back(glbTrackM->numberOfValidHits()                          );
                B_mu_pix2       ->push_back(glbTrackM->hitPattern().numberOfValidPixelHits()        );

                B_mass          ->push_back(B_mass_cjp_tmp                                          );
                B_mass_c0       ->push_back(B_mass_c0_tmp                                           );
                B_Prob_c0       ->push_back(B_Prob_c0_tmp                                           );
                B_Prob          ->push_back(B_Prob_tmp                                              );
                B_ka_charg      ->push_back(iTrack1->charge()                                       );
                B_px            ->push_back(bCandMC->currentState().globalMomentum().x()            );
                B_py            ->push_back(bCandMC->currentState().globalMomentum().y()            );
                B_pz            ->push_back(bCandMC->currentState().globalMomentum().z()            );
                B_ka_px         ->push_back(p4ka1.Px()                                              );
                B_ka_py         ->push_back(p4ka1.Py()                                              );
                B_ka_pz         ->push_back(p4ka1.Pz()                                              );
                B_ka_px_CV      ->push_back(kaCandMC_p.x()                                          );
                B_ka_py_CV      ->push_back(kaCandMC_p.y()                                          );
                B_ka_pz_CV      ->push_back(kaCandMC_p.z()                                          );
                B_DecayVtxX     ->push_back(B_vtx->position().x()                                   );
                B_DecayVtxY     ->push_back(B_vtx->position().y()                                   );
                B_DecayVtxZ     ->push_back(B_vtx->position().z()                                   );
                B_DecayVtxXE    ->push_back(B_vtx->error().cxx()                                    );
                B_DecayVtxYE    ->push_back(B_vtx->error().cyy()                                    );
                B_DecayVtxZE    ->push_back(B_vtx->error().czz()                                    );
                B_ka_hit        ->push_back(iTrack1->numberOfHits()                                 );
                B_ka_pix        ->push_back(iTrack1->numberOfPixelHits()                            );
                B_ka_ips        ->push_back(fabs(iTrack1->dxy(bestVtxBSIP.position())) / (0.000001 + fabs(iTrack1->dxyError())) );
                B_pvdistsignif2_Cjp ->push_back(XB_pvdistsignif2_Cjp_tmp                            );
                B_pvcos2_Cjp        ->push_back(XB_pvcos2_Cjp_tmp                                   );

                ////
                PV_becos_XX     ->push_back( pVtxBSIPX_temp                                         );
                PV_becos_YY     ->push_back( pVtxBSIPY_temp                                         );
                PV_becos_ZZ     ->push_back( pVtxBSIPZ_temp                                         );
                PV_becos_EX     ->push_back( pVtxBSIPXE_temp                                        );
                PV_becos_EY     ->push_back( pVtxBSIPYE_temp                                        );
                PV_becos_EZ     ->push_back( pVtxBSIPZE_temp                                        );
                PV_becos_CL     ->push_back( pVtxBSIPCL_temp                                        );
                PV_becos_dN     ->push_back( pVtxBSIPdN_temp                                        );

                trig0           ->push_back( TriggerResults[0]                                      );
                trig1           ->push_back( TriggerResults[1]                                      );
                trig2           ->push_back( TriggerResults[2]                                      );
                trig3           ->push_back( TriggerResults[3]                                      );
                trig4           ->push_back( TriggerResults[4]                                      );
                trig5           ->push_back( TriggerResults[5]                                      );
                trig6           ->push_back( TriggerResults[6]                                      );
                trig7           ->push_back( TriggerResults[7]                                      );
                trig8           ->push_back( TriggerResults[8]                                      );
                trig9           ->push_back( TriggerResults[9]                                      );
                trig10          ->push_back( TriggerResults[10]                                     );
                trig11          ->push_back( TriggerResults[11]                                     );
                trig12          ->push_back( TriggerResults[12]                                     );
                trig13          ->push_back( TriggerResults[13]                                     );

                trig0_fire      ->push_back( TriggersFired[0]                                       );
                trig1_fire      ->push_back( TriggersFired[1]                                       );
                trig2_fire      ->push_back( TriggersFired[2]                                       );
                trig3_fire      ->push_back( TriggersFired[3]                                       );
                trig4_fire      ->push_back( TriggersFired[4]                                       );
                trig5_fire      ->push_back( TriggersFired[5]                                       );
                trig6_fire      ->push_back( TriggersFired[6]                                       );
                trig7_fire      ->push_back( TriggersFired[7]                                       );
                trig8_fire      ->push_back( TriggersFired[8]                                       );
                trig9_fire      ->push_back( TriggersFired[9]                                       );
                trig10_fire     ->push_back( TriggersFired[10]                                      );
                trig11_fire     ->push_back( TriggersFired[11]                                      );
                trig12_fire     ->push_back( TriggersFired[12]                                      );
                trig13_fire     ->push_back( TriggersFired[13]                                      );


                nCand ++;
                B_candidate_init.clear();
                B_candidate.clear();
            } muonParticles.clear();
        } // muonN
    } // muonP


// ===================== END OF EVENT : WRITE ETC ++++++++++++++++++++++

    if (nCand > 0)
    {
        cout << "_____________________ SUCCESS!!!! _______________________" << endl;
        N_written_events++;
        cout << N_written_events << " candidates are written to the file now " << endl;
        cout << "Trigger: " << strfired << endl;
        cout << endl;

        wwtree->Fill();
    }
    // nCand > 0

    B_J_Prob->clear();          B_J_mass->clear();
    B_J_px->clear();            B_J_py->clear();            B_J_pz->clear();
    B_J_DecayVtxX->clear();     B_J_DecayVtxY->clear();     B_J_DecayVtxZ->clear();
    B_J_DecayVtxXE->clear();    B_J_DecayVtxYE->clear();    B_J_DecayVtxZE->clear();
//
    B_mu_px1_cjp->clear();      B_mu_py1_cjp->clear();      B_mu_pz1_cjp->clear();
    B_mu_px1->clear();          B_mu_py1->clear();          B_mu_pz1->clear();
    B_mu_ips1->clear();         B_mu_hit1->clear();         B_mu_pix1->clear();

    B_mu_px2_cjp->clear();      B_mu_py2_cjp->clear();      B_mu_pz2_cjp->clear();
    B_mu_px2->clear();          B_mu_py2->clear();          B_mu_pz2->clear();
    B_mu_ips2->clear();         B_mu_hit2->clear();         B_mu_pix2->clear();
//
    B_mass->clear();       B_mass_c0->clear();    B_Prob_c0->clear(); B_Prob->clear(); B_ka_charg->clear();
    B_px->clear();         B_py->clear();         B_pz->clear();
    B_ka_px->clear();      B_ka_py->clear();      B_ka_pz->clear();
    B_ka_px_CV->clear();   B_ka_py_CV->clear();   B_ka_pz_CV->clear();
    B_DecayVtxX ->clear();      B_DecayVtxY ->clear();      B_DecayVtxZ ->clear();
    B_DecayVtxXE->clear();      B_DecayVtxYE->clear();      B_DecayVtxZE->clear();
    B_ka_hit->clear();     B_ka_pix->clear();     B_ka_ips->clear();
    B_pvdistsignif2_Cjp->clear(); B_pvcos2_Cjp->clear();

    PV_becos_XX->clear();   PV_becos_YY->clear();   PV_becos_ZZ->clear();
    PV_becos_EX->clear();   PV_becos_EY->clear();   PV_becos_EZ->clear();
    PV_becos_CL->clear();   PV_becos_dN->clear();

    trig0->clear();      trig1->clear();      trig2->clear();
    trig3->clear();      trig4->clear();      trig5->clear();
    trig6->clear();      trig7->clear();      trig8->clear();
    trig9->clear();      trig10->clear();     trig11->clear();
    trig12->clear();     trig13->clear();

    trig0_fire->clear();      trig1_fire->clear();      trig2_fire->clear();
    trig3_fire->clear();      trig4_fire->clear();      trig5_fire->clear();
    trig6_fire->clear();      trig7_fire->clear();      trig8_fire->clear();
    trig9_fire->clear();      trig10_fire->clear();     trig11_fire->clear();
    trig12_fire->clear();     trig13_fire->clear();

}

bool Xb_frame::IsTheSame(const pat::GenericParticle& tk, const pat::Muon& mu){
    double DeltaEta = fabs(mu.eta() - tk.eta());
    double DeltaP   = fabs(mu.p() - tk.p());
    if (DeltaEta < 0.001 && DeltaP < 0.001) return true;
    return false;
}

// ------------ method called once each job just before starting event loop  ------------
void Xb_frame::beginJob()
{
    using namespace std;
    using namespace reco;
//
    cout << "------------------------------->>>>> Begin Job" << endl;

    f = new TFile(fileName.c_str(), "RECREATE");
    wwtree  = new TTree("wztree", "muons tree");

    wwtree->Branch("nCand"              , &nCand            , "nCand/I"     );

    wwtree->Branch("run"                , &run              , "run/I"       );
    wwtree->Branch("event"              , &event            , "event/I"     );
    wwtree->Branch("lumi"               , &lumi             , "lumi/F"      );

    wwtree->Branch("numPV"              , &numPV            , "numPV/I"     );
    wwtree->Branch("numTrack"           , &numTrack         , "numTrack/I"  );
    wwtree->Branch("numMuon"            , &numMuon          , "numMuon/I"   );
    wwtree->Branch("numV0"              , &numV0            , "numV0/I"     );

    wwtree->Branch("B_J_Prob"         , &B_J_Prob         );
    wwtree->Branch("B_J_mass"         , &B_J_mass         );
    wwtree->Branch("B_J_px"           , &B_J_px           );
    wwtree->Branch("B_J_py"           , &B_J_py           );
    wwtree->Branch("B_J_pz"           , &B_J_pz           );
    wwtree->Branch("B_J_DecayVtxX"    , &B_J_DecayVtxX    );
    wwtree->Branch("B_J_DecayVtxY"    , &B_J_DecayVtxY    );
    wwtree->Branch("B_J_DecayVtxZ"    , &B_J_DecayVtxZ    );
    wwtree->Branch("B_J_DecayVtxXE"   , &B_J_DecayVtxXE   );
    wwtree->Branch("B_J_DecayVtxYE"   , &B_J_DecayVtxYE   );
    wwtree->Branch("B_J_DecayVtxZE"   , &B_J_DecayVtxZE   );

    wwtree->Branch("B_mu_px1_cjp"       , &B_mu_px1_cjp       );
    wwtree->Branch("B_mu_py1_cjp"       , &B_mu_py1_cjp       );
    wwtree->Branch("B_mu_pz1_cjp"       , &B_mu_pz1_cjp       );
    wwtree->Branch("B_mu_px1"           , &B_mu_px1           );
    wwtree->Branch("B_mu_py1"           , &B_mu_py1           );
    wwtree->Branch("B_mu_pz1"           , &B_mu_pz1           );
    wwtree->Branch("B_mu_ips1"          , &B_mu_ips1          );
    wwtree->Branch("B_mu_hit1"          , &B_mu_hit1          );
    wwtree->Branch("B_mu_pix1"          , &B_mu_pix1          );

    wwtree->Branch("B_mu_px2_cjp"       , &B_mu_px2_cjp       );
    wwtree->Branch("B_mu_py2_cjp"       , &B_mu_py2_cjp       );
    wwtree->Branch("B_mu_pz2_cjp"       , &B_mu_pz2_cjp       );
    wwtree->Branch("B_mu_px2"           , &B_mu_px2           );
    wwtree->Branch("B_mu_py2"           , &B_mu_py2           );
    wwtree->Branch("B_mu_pz2"           , &B_mu_pz2           );
    wwtree->Branch("B_mu_ips2"          , &B_mu_ips2          );
    wwtree->Branch("B_mu_hit2"          , &B_mu_hit2          );
    wwtree->Branch("B_mu_pix2"          , &B_mu_pix2          );

    wwtree->Branch("B_mass"             , &B_mass             );
    wwtree->Branch("B_mass_c0"          , &B_mass_c0          );
    wwtree->Branch("B_Prob_c0"          , &B_Prob_c0          );
    wwtree->Branch("B_Prob"             , &B_Prob             );
    wwtree->Branch("B_ka_charg"         , &B_ka_charg         );
    wwtree->Branch("B_px"               , &B_px               );
    wwtree->Branch("B_py"               , &B_py               );
    wwtree->Branch("B_pz"               , &B_pz               );
    wwtree->Branch("B_ka_px"            , &B_ka_px            );
    wwtree->Branch("B_ka_py"            , &B_ka_py            );
    wwtree->Branch("B_ka_pz"            , &B_ka_pz            );
    wwtree->Branch("B_ka_px_CV"         , &B_ka_px_CV         );
    wwtree->Branch("B_ka_py_CV"         , &B_ka_py_CV         );
    wwtree->Branch("B_ka_pz_CV"         , &B_ka_pz_CV         );
    wwtree->Branch("B_DecayVtxX"        , &B_DecayVtxX        );
    wwtree->Branch("B_DecayVtxY"        , &B_DecayVtxY        );
    wwtree->Branch("B_DecayVtxZ"        , &B_DecayVtxZ        );
    wwtree->Branch("B_DecayVtxXE"       , &B_DecayVtxXE       );
    wwtree->Branch("B_DecayVtxYE"       , &B_DecayVtxYE       );
    wwtree->Branch("B_DecayVtxZE"       , &B_DecayVtxZE       );
    wwtree->Branch("B_ka_hit"           , &B_ka_hit           );
    wwtree->Branch("B_ka_pix"           , &B_ka_pix           );
    wwtree->Branch("B_ka_ips"           , &B_ka_ips           );
    wwtree->Branch("B_pvdistsignif2_Cjp", &B_pvdistsignif2_Cjp);
    wwtree->Branch("B_pvcos2_Cjp"       , &B_pvcos2_Cjp       );

    wwtree->Branch("PV_becos_XX"        , &PV_becos_XX        );
    wwtree->Branch("PV_becos_YY"        , &PV_becos_YY        );
    wwtree->Branch("PV_becos_ZZ"        , &PV_becos_ZZ        );
    wwtree->Branch("PV_becos_EX"        , &PV_becos_EX        );
    wwtree->Branch("PV_becos_EY"        , &PV_becos_EY        );
    wwtree->Branch("PV_becos_EZ"        , &PV_becos_EZ        );
    wwtree->Branch("PV_becos_CL"        , &PV_becos_CL        );
    wwtree->Branch("PV_becos_dN"        , &PV_becos_dN        );

    wwtree->Branch("trig0"              , &trig0              );
    wwtree->Branch("trig1"              , &trig1              );
    wwtree->Branch("trig2"              , &trig2              );
    wwtree->Branch("trig3"              , &trig3              );
    wwtree->Branch("trig4"              , &trig4              );
    wwtree->Branch("trig5"              , &trig5              );
    wwtree->Branch("trig6"              , &trig6              );
    wwtree->Branch("trig7"              , &trig7              );
    wwtree->Branch("trig8"              , &trig8              );
    wwtree->Branch("trig9"              , &trig9              );
    wwtree->Branch("trig10"             , &trig10             );
    wwtree->Branch("trig11"             , &trig11             );
    wwtree->Branch("trig12"             , &trig12             );
    wwtree->Branch("trig13"             , &trig13             );

    wwtree->Branch("trig0_fire"         , &trig0_fire         );
    wwtree->Branch("trig1_fire"         , &trig1_fire         );
    wwtree->Branch("trig2_fire"         , &trig2_fire         );
    wwtree->Branch("trig3_fire"         , &trig3_fire         );
    wwtree->Branch("trig4_fire"         , &trig4_fire         );
    wwtree->Branch("trig5_fire"         , &trig5_fire         );
    wwtree->Branch("trig6_fire"         , &trig6_fire         );
    wwtree->Branch("trig7_fire"         , &trig7_fire         );
    wwtree->Branch("trig8_fire"         , &trig8_fire         );
    wwtree->Branch("trig9_fire"         , &trig9_fire         );
    wwtree->Branch("trig10_fire"        , &trig10_fire        );
    wwtree->Branch("trig11_fire"        , &trig11_fire        );
    wwtree->Branch("trig12_fire"        , &trig12_fire        );
    wwtree->Branch("trig13_fire"        , &trig13_fire        ); 

}

// ------------ method called once each job just after ending the event loop  ------------
void
Xb_frame::endJob()
{

    using namespace std;
    cout << "------------------------------->>>>> Ending Job" << endl;
    cout << endl;
    cout <<  "total " << N_written_events << " candidates were written to the file" << endl;
    cout << "------------------------------->>>>> End Job" << endl;

    f->WriteTObject(wwtree);
    delete wwtree;
    f->Close();

}

// ------------ method fills 'descriptions' with the allowed parameters for the module  ------------
void
Xb_frame::fillDescriptions(edm::ConfigurationDescriptions& descriptions) {
    //The following says we do not know what parameters are allowed so do no validation
    // Please change this to state exactly what you do use, even if it is no parameters
    edm::ParameterSetDescription desc;
    desc.setUnknown();
    descriptions.addDefault(desc);
}

//define this as a plug-in
DEFINE_FWK_MODULE(Xb_frame);
