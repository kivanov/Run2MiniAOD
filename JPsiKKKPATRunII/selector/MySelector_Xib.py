#
# ------------------- IMPORT LIBRARIES, FILES AND ETC -------------------
#
#
# Importing PyROOT and libraries
#
import ROOT
import glob
import inspect
import numpy as np
from math import sqrt

import variables as v


def retrieve_name(var):
    """
    Gets the name of var. Does it from the out most frame inner-wards.
    :param var: variable to get name from.
    :return: string
    """
    for fi in reversed(inspect.stack()):
        names = [var_name for var_name, var_val in fi.frame.f_locals.items() if var_val is var]
        if len(names) > 0:
            return names[0]


# Locating CRAB files
#

main_path = '/afs/cern.ch/work/k/kivanov/Analyzers/analyzes/CMSSW_10_6_12/src/13TeVAOD_new/JPsiKKKPATRunII/'
main_path = '/eos/user/k/kivanov/Bfinder/Mini/'

# for 2016 data
#
# MyFileNames = glob.glob(main_path + '2016/crab_projects_Bfinder_201*/crab*/results/*.root')

# for 2017 data
#
# MyFileNames = glob.glob(main_path + '2017/crab_projects_Bfinder_201*/crab*/results/*.root')

# for 2018 data
#
# MyFileNames = glob.glob(main_path + '2018/crab_projects_Bfinder_201*/crab*/results/*.root')

# for all data
#
# MyFileNames = glob.glob(main_path + '2018/crab_projects_Bfinder_2018_Ds_test_try/crab_Bfinder_*/results/*.root')
MyFileNames = glob.glob(main_path + 'BuToJpsiK_test/201*/Charmonium/CRAB3_Bfinder/21*/000*/*.root')
print('We have', len(MyFileNames), 'files')
#
# path_out = '/afs/cern.ch/work/k/kivanov/private/data_from_MySelector/Cbarion-2016-2018/'
path_out = '/eos/home-k/kivanov/data_from_MySelector/BsToDs/'

# For local tests etc
#
local_path = '/Users/kirillivanov/Documents/Science/Cb/lxplus/13TeVAOD_new/JPsiKKKPATRunII/selector/data/'
local_MyFileNames = glob.glob(local_path + '/crab*/results/*.root')
#
# !!!!!!!!!!!! SETTINGS !!!!!!!!!!!!
#
lxplus = True
N = '0'

# Creating outputs
#
if lxplus:
    _fileOUT       = path_out + N + '_BuToJpsiK.root'
else:
    _fileOUT       = local_path + N + '_l_CbToCmumu-2016-2017-2018.root'
#
#
split_ = False
if split_:
    __aa, __bb = 0, 40
    MyFileNames = MyFileNames[__aa: __bb]
    _fileOUT       = path_out + N + f'_DsToPhiKKPi-{__aa}_{__bb}.root'
# ------------------- CREATING TTREE, VARS -------------------
#
#

if not lxplus:
    ch = ROOT.TChain('wztree')
    for fName in local_MyFileNames:
        ch.Add(fName)
    print('Get', len(local_MyFileNames), 'files;  chain created')
else:
    ch = ROOT.TChain('wztree')
    for fName in MyFileNames:
        ch.Add(fName)
    if split_:
        print('Get', len(MyFileNames), f'files from {__aa} to {__bb};  chain created')
    else:
        print('Get', len(MyFileNames), 'files;  chain created')

fileOUT = ROOT.TFile(_fileOUT, 'recreate')

mytree_Right = ROOT.TTree('mytree_Right', 'mytree_Right')
# mytree_Wrong = ROOT.TTree('mytree_Wrong', 'mytree_Wrong')

nEvt = ch.GetEntries()
print('Get entries: from', 0, 'to', nEvt - 1, 'of', ch.GetEntries() - 1, 'existing')

NOUT, BBB = 0, 0

# List of TLorentzVectors used to create vars from BFinder data
#
[ MU1P4, MU2P4, MU1P4_Cjp, MU2P4_Cjp, JPP4,
  B_kaP4, B_kaP4_CV, B_kaP4_CVpi, BP4,] = [ROOT.TLorentzVector() for i in range(9)]
_TV3zero = ROOT.TVector3(0, 0, 0)

# List of vars we need for our analyze
#
_MY_VARS_ = [SAMEEVENT, run,

             B_mass_old, B_mass,
             B_pt, B_eta, B_vtxprob, B_pvdist, B_pvdistxy,
             B_pvcos2, B_pvcos3,
             B_pvdistsignif2, B_pvdistsignif3,
             B_ka_pt, B_ka_pt_CV, B_ka_eta,
             B_lamk_mass, B_ka_charge,
             B_ka_hit, B_ka_pix, B_ka_ips,

             MU_pt,
             B_J_mass_Cmumu, B_J_mas1_Cmumu, B_J_mas2, B_J_pt_Cmumu,
             B_J_Prob_Cmumu, B_J_pvcos2_Cmumu, B_J_pvdistsignif2_Cmumu,

             #   0           1                  2                    3
             TRIG_Jp25, TRIG_Jp20Seag, TRIG_JpTrkDisplaced, TRIG_JpTrkTrkDisplaced,
             TRIG_Jp43isplaced,  # 2017, 4
             TRIG_Jp43,  # 2018, 5
             TRIG_Jp4Displaced, #6

             ## Psi2S Triggers now
             #    7             8                  9
             TRIG_Psip18, TRIG_Psip10Seag, TRIG_PripTrDisplaced,

             #  10           11
             TRIG_JpMu, TRIG_JpPhiMM,
             TRIG_JpPhiKKv2,  # 2017, 12
             TRIG_JpPhiKKv4,  # 2018 13

             ] = [np.zeros(1, dtype=float) for i in range(43)]

# Creating vars-Branches of our TTree
#
for _var_ in _MY_VARS_:
    mytree_Right.Branch(retrieve_name(_var_), _var_, retrieve_name(_var_) + '/D')
    # mytree_Wrong.Branch(retrieve_name(_var_), _var_, retrieve_name(_var_) + '/D')
#
#
# ------------------- LOOP FOR SELECTING EVENTS AND FILLING VARS  -------------------
#
#
for evt in range(0, nEvt):
    ##
    if ch.GetEntry(evt) <= 0:
        break
    nCand = ch.nCand
    if nCand != len(ch.B_mass):
        print('AAAAAAAAAAAAAAAAAAA', nCand, ';', len(ch.B_mass))
    #
    for cand in range(nCand):

        # _ _ _ _ _ _ _ _ _ _ _ _ _ STARTING CREATING TVECTOR _ _ _ _ _ _ _ _ _ _ _ _ _
        try:
            #
            # ____________________________ Bu ____________________________
            #
            BVER       = ROOT.TVector3(ch.B_DecayVtxX[cand], ch.B_DecayVtxY[cand], ch.B_DecayVtxZ[cand])
            BVE        = ROOT.TVector3(sqrt(abs(ch.B_DecayVtxXE[cand])), sqrt(abs(ch.B_DecayVtxYE[cand])),
                                       sqrt(abs(ch.B_DecayVtxZE[cand])))

            BP4        .SetXYZM (ch.B_px[cand]      , ch.B_py[cand]      , ch.B_pz[cand]      , ch.B_mass[cand])
            B_kaP4     .SetXYZM (ch.B_ka_px[cand]   , ch.B_ka_py[cand]   , ch.B_ka_pz[cand]   , v.PDG_KAON_MASS)
            B_kaP4_CV  .SetXYZM (ch.B_ka_px_CV[cand], ch.B_ka_py_CV[cand], ch.B_ka_pz_CV[cand], v.PDG_KAON_MASS)
            BP3        = BP4.Vect()

            # Assuming kaon is a pion participating X --> J/psi pi+ reflection ?...
            B_kaP4_CVpi.SetXYZM (ch.B_ka_px_CV[cand], ch.B_ka_py_CV[cand], ch.B_ka_pz_CV[cand], v.PDG_PION_MASS)
            #

            #____________________________ PV ____________________________
            #
            PV          = ROOT.TVector3(ch.PV_becos_XX[cand], ch.PV_becos_YY[cand], ch.PV_becos_ZZ[cand])
            PVE         = ROOT.TVector3(sqrt(ch.PV_becos_EX[cand]), sqrt(ch.PV_becos_EY[cand]), sqrt(ch.PV_becos_EZ[cand]))
            #
            # ____________________________ mu ____________________________
            #
            MU1P4       .SetXYZM(ch.B_mu_px1[cand]    , ch.B_mu_py1[cand]    , ch.B_mu_pz1[cand]    , v.PDG_MUON_MASS)
            MU2P4       .SetXYZM(ch.B_mu_px2[cand]    , ch.B_mu_py2[cand]    , ch.B_mu_pz2[cand]    , v.PDG_MUON_MASS)
            MU1P4_Cjp   .SetXYZM(ch.B_mu_px1_cjp[cand], ch.B_mu_py1_cjp[cand], ch.B_mu_pz1_cjp[cand], v.PDG_MUON_MASS)
            MU2P4_Cjp   .SetXYZM(ch.B_mu_px2_cjp[cand], ch.B_mu_py2_cjp[cand], ch.B_mu_pz2_cjp[cand], v.PDG_MUON_MASS)
            #
            # ____________________________ J\psi ____________________________
            #
            JPV     = ROOT.TVector3(ch.B_J_DecayVtxX[cand], ch.B_J_DecayVtxY[cand], ch.B_J_DecayVtxZ[cand])
            JPP3    = ROOT.TVector3(ch.B_J_px[cand], ch.B_J_py[cand], ch.B_J_pz[cand])
            JPVE    = ROOT.TVector3(sqrt(ch.B_J_DecayVtxXE[cand]), sqrt(ch.B_J_DecayVtxYE[cand]),
                                    sqrt(ch.B_J_DecayVtxZE[cand]))
            JPP4    .SetXYZM(ch.B_J_px[cand], ch.B_J_py[cand], ch.B_J_pz[cand], ch.B_J_mass[cand])
            #
        except ValueError:
            print('ValueError')
        # _ _ _ _ _ _ _ _ _ _ _ _ _ ENDING CREATING TVECTOR _ _ _ _ _ _ _ _ _ _ _ _ _
        #
        #
        # _ _ _ _ _ _ _ _ _ _ _ _ _ STARING WRITING VARS TO TTREE _ _ _ _ _ _ _ _ _ _ _ _ _

        run[0]              = ch.run

        #

        # ____________________________ Bu-meson ____________________________
        #
        # - - - mass - - -
        #
        B_mass[0]          = BP4.M() - (MU1P4_Cjp + MU2P4_Cjp).M() + v.PDG_JPSI_MASS
        B_mass_old[0]      = ch.B_mass[cand]
        # B_mass_sum1[0]     = (C_piP4 + PHIP4).M() - PHIP4.M() + v.PDG_PHI_MASS
        # B_mass_sum2[0]     = (C_piP4_CV + PHIP4).M() - PHIP4.M() + v.PDG_PHI_MASS

        # if B_mass_old[0] < 2.35:
        #     continue
        # if B_mass_old[0] > 2.60:
        #     continue
        #
        # - - - any - - -
        #
        B_pt[0]            = BP4.Pt()
        B_eta[0]           = BP4.Eta()
        B_vtxprob[0]       = ch.B_Prob[cand]
        B_pvdist[0]        = (BVER - PV).Mag()
        # C_CBdist[0]       = (CVER - XBV_Cjp).Mag()
        B_pvdistxy[0]      = sqrt( (BVER - PV).X() ** 2 + (BVER - PV).Y() ** 2 )

        if B_pt[0] < 10:
            continue
        if B_vtxprob[0] < 0.01:
            continue
        #
        # - - - cos - - -
        #
        B_pvcos2[0]        = v.DirectionCos2(BVER - PV     , BP3)
        B_pvcos3[0]        = v.DirectionCos3(BVER - PV     , BP3)

        if B_pvcos2[0] < 0.9:
            continue
        # if B_Ccos2[0] < 0.9:
        #     continue
        #
        # - - - DetachSign - - -
        #
        B_pvdistsignif2[0] = v.DetachSignificance2(BVER - PV     , PVE     , BVE)
        B_pvdistsignif3[0] = v.DetachSignificance3(BVER - PV     , PVE     , BVE)

        if B_pvdistsignif2[0] < 2:
            continue
        # if B_Cdistsignif2[0] < 3:
        #     continue
        # if B_pvdistsignif2[0] > 500:
        #     continue
        #
        # - - - Pion_B  - - -
        #
        B_ka_pt_CV[0]      = B_kaP4_CV.Pt()
        B_ka_pix[0]        = ch.B_ka_pix[cand]
        B_ka_hit[0]        = ch.B_ka_hit[cand]
        B_ka_pt[0]         = B_kaP4.Pt()
        B_ka_eta[0]        = B_kaP4.Eta()
        B_ka_ips[0]        = ch.B_ka_ips[cand]
        # B_pi_HPT[0]        = ch.B_pi_HPT[cand]
        B_ka_charge[0]     = ch.B_ka_charg[cand]

        if B_ka_ips[0] < 0.4:
            continue
        # Assuming X --> Ds K+ reflection ?...
        B_lamk_mass[0]     = (JPP4 + B_kaP4_CVpi).M() - (MU1P4_Cjp + MU2P4_Cjp).M() + v.PDG_JPSI_MASS
        #


        # _________________________________ J/psi __________________________________
        #
        # - - - muon - - -
        #
        MU_pt[0]                    = min(MU1P4.Pt(), MU2P4.Pt())
        #
        # - - - mass - - -
        #
        B_J_mass_Cmumu[0]          = ch.B_J_mass[cand]
        #
        # - - - any - - -
        #
        B_J_pt_Cmumu[0]            = JPP4.Pt()
        B_J_Prob_Cmumu[0]          = ch.B_J_Prob[cand]
        B_J_pvcos2_Cmumu[0]        = v.DirectionCos2 (JPV - PV, JPP3 )
        B_J_pvdistsignif2_Cmumu[0] = v.DetachSignificance2( JPV - PV, PVE, JPVE)
        #
        #
        # ____________________________ Triggers ____________________________
        #
        #
        TRIG_Jp25             [0] =  ch.trig0_fire[cand]  and ch.trig0[cand]
        TRIG_Jp20Seag         [0] =  ch.trig1_fire[cand]  and ch.trig1[cand]
        TRIG_JpTrkDisplaced   [0] =  ch.trig2_fire[cand]  and ch.trig2[cand]
        TRIG_JpTrkTrkDisplaced[0] =  ch.trig3_fire[cand]  and ch.trig3[cand]
        TRIG_Jp43isplaced     [0] =  ch.trig4_fire[cand]  and ch.trig4[cand]
        TRIG_Jp43             [0] =  ch.trig5_fire[cand]  and ch.trig5[cand]
        TRIG_Jp4Displaced     [0] =  ch.trig6_fire[cand]  and ch.trig6[cand]

        TRIG_Psip18           [0] =  ch.trig7_fire[cand]  and ch.trig7[cand]
        TRIG_Psip10Seag       [0] =  ch.trig8_fire[cand]  and ch.trig8[cand]
        TRIG_PripTrDisplaced  [0] =  ch.trig9_fire[cand]  and ch.trig9[cand]

        TRIG_JpMu             [0] =  ch.trig10_fire[cand] and ch.trig10[cand]
        TRIG_JpPhiMM          [0] =  ch.trig11_fire[cand] and ch.trig11[cand]
        TRIG_JpPhiKKv2        [0] =  ch.trig12_fire[cand] and ch.trig12[cand]
        TRIG_JpPhiKKv4        [0] =  ch.trig13_fire[cand] and ch.trig13[cand]
        #
        # ____________________________ End and writing ____________________________
        #
        SAMEEVENT[0] = 0
        if BBB > -1:
            SAMEEVENT[0] = 1


        mytree_Right.Fill()

        NOUT += 1
        BBB = cand

        # _ _ _ _ _ _ _ _ _ _ _ _ _ ENDING WRITING VARS TO TTREE _ _ _ _ _ _ _ _ _ _ _ _ _

    BBB = -1

    # printout progress
    #
    if evt % 5000 == 0:
        perc = str(int(100 * evt / (nEvt - 0)))
        print(f'[{perc}', ' ' * (2 - len(perc)), f'%] entry: {evt}, saved: {NOUT}.')

fileOUT.Write()
