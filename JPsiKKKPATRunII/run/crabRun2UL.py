from CRABClient.UserUtilities import config
config = config()

config.section_("General")
config.General.requestName = 'Bfinder'
config.General.workArea = 'crab_projects_Bfinder'
config.General.transferLogs = False

config.section_("JobType")
config.JobType.pluginName = 'Analysis'
config.JobType.allowUndistributedCMSSW = True
config.JobType.psetName = 'ConfFile_cfg_Run2UL.py'

config.section_("Data")
config.Data.inputDataset = '/Charmonium/Run2018A-12Nov2019_UL2018_rsb-v1/MINIAOD'
config.Data.inputDBS =    'global'
config.Data.splitting = 'LumiBased'
##config.Data.splitting = 'Automatic'

config.Data.publication = False
config.Data.ignoreLocality = True
config.Data.publishDBS = 'phys03'

config.Data.outputDatasetTag = 'CRAB3_Bfinder'
config.Data.outLFNDirBase = '/store/user/kivanov/Bfinder/'

config.section_("Site")
# config.Site.storageSite = 'T2_RU_IHEP'
config.Site.storageSite = 'T3_CH_CERNBOX'
config.Site.whitelist = ['T2_AT_Vienna', 'T2_BE_*', 'T2_BR_*', 'T2_CH_*', 'T2_DE_*', 'T2_ES_*', 'T2_FR_*',  'T2_HU_*', 'T2_IT_*', 'T2_PL_*', 'T2_PT_NCG_Lisbon', 'T2_TR_METU']

DS_names = [
    '/Charmonium/Run2018A-12Nov2019_UL2018_rsb-v1/MINIAOD',
    '/Charmonium/Run2018B-12Nov2019_UL2018-v1/MINIAOD',
    '/Charmonium/Run2018C-12Nov2019_UL2018_rsb_v2-v2/MINIAOD',
    '/Charmonium/Run2018D-12Nov2019_UL2018-v1/MINIAOD',

    '/Charmonium/Run2017B-09Aug2019_UL2017-v1/MINIAOD',
    '/Charmonium/Run2017C-09Aug2019_UL2017-v1/MINIAOD',
    '/Charmonium/Run2017D-09Aug2019_UL2017-v1/MINIAOD',
    '/Charmonium/Run2017E-09Aug2019_UL2017-v1/MINIAOD',
    '/Charmonium/Run2017F-09Aug2019_UL2017-v1/MINIAOD',

    '/Charmonium/Run2016B-21Feb2020_ver2_UL2016_HIPM-v1/MINIAOD',
    '/Charmonium/Run2016C-21Feb2020_UL2016_HIPM-v1/MINIAOD',
    '/Charmonium/Run2016D-21Feb2020_UL2016_HIPM-v1/MINIAOD',
    '/Charmonium/Run2016E-21Feb2020_UL2016_HIPM-v1/MINIAOD',
    '/Charmonium/Run2016F-21Feb2020_UL2016_HIPM-v1/MINIAOD',
    '/Charmonium/Run2016F-21Feb2020_UL2016-v1/MINIAOD',
    '/Charmonium/Run2016G-21Feb2020_UL2016-v1/MINIAOD',
    '/Charmonium/Run2016H-21Feb2020_UL2016-v1/MINIAOD',
]


if __name__ == '__main__':
    import sys
    from CRABAPI.RawCommand import crabCommand
    from httplib import HTTPException
    task = 'Xib2_new_try_2'
    year = '2017'
    year = str(sys.argv[2])
    print 'Year set to ' + year + '.\nunitsPerJob ~ 10 for maximum splitting\nHave you done scram b -j8?!'

    ####
    ####
    units_per_job = 30
    #
    n = 0
    if len(sys.argv) >= 2:
        n = int(sys.argv[1])
        #
    else:
        print ' ADD A NUMBER TO SET DATASET ... 0 - %i'%(len(DS_names))
        exit(0)

    def submit(cfg):
        try:
            print crabCommand('submit', config = cfg)
        except HTTPException, hte:
            print hte.headers

    dset = DS_names[n]
    if   n == 0:
        config.General.requestName = 'Bfinder_' + task + '_' + dset[15:44]
    elif n == 2:
        config.General.requestName = 'Bfinder_' + task + '_' + dset[15:47]
    elif (n > 8 and n < 14):
        if n == 9:
            config.General.requestName = 'Bfinder_' + task + '_' + dset[15:50]
        else:
            config.General.requestName = 'Bfinder_' + task + '_' + dset[15:45]
    else:
        config.General.requestName = 'Bfinder_' + task + '_' + dset[15:40]

    config.General.workArea = '../' + year + '/crab_projects_Bfinder_' + year + '_' + task
    config.Data.inputDataset = dset
    config.Data.outLFNDirBase = '/store/user/kivanov/Bfinder/' + task + '/' + year + '/'
    print '\n', config.General.requestName
    print config.General.workArea
    print dset, '\n'
    #     #
    #    lumi_mask = 'https://cms-service-dqm.web.cern.ch/cms-service-dqm/CAF/certification/Collisions17/13TeV/PromptReco/Cert_294927-297723_13TeV_PromptReco_Collisions17_JSON_MuonPhys.txt'
    #    lumi_mask = 'https://cms-service-dqm.web.cern.ch/cms-service-dqm/CAF/certification/Collisions17/13TeV/DCSOnly/json_DCSONLY.txt'
    #    lumi_mask = 'https://cms-service-dqm.web.cern.ch/cms-service-dqm/CAF/certification/Collisions18/13TeV/PromptReco/Cert_314472-321221_13TeV_PromptReco_Collisions18_JSON_MuonPhys.txt'
    #    lumi_mask = 'https://cms-service-dqm.web.cern.ch/cms-service-dqm/CAF/certification/Collisions18/13TeV/PromptReco/Cert_314472-321777_13TeV_PromptReco_Collisions18_JSON_MuonPhys.txt'
    #    lumi_mask = 'https://cms-service-dqm.web.cern.ch/cms-service-dqm/CAF/certification/Collisions18/13TeV/PromptReco/Cert_314472-322057_13TeV_PromptReco_Collisions18_JSON_MuonPhys.txt '
    # lumi_mask = "https://cms-service-dqm.web.cern.ch/cms-service-dqm/CAF/certification/Collisions18/13TeV/PromptReco/Cert_314472-325175_13TeV_PromptReco_Collisions18_JSON_MuonPhys.txt"
    if year == '2018':
        lumi_mask = "/afs/cern.ch/cms/CAF/CMSCOMM/COMM_DQM/certification/Collisions18/13TeV/Legacy_2018/Cert_314472-325175_13TeV_Legacy2018_Collisions18_JSON_MuonPhys.txt"
    elif year == '2017':
        lumi_mask = '/afs/cern.ch/cms/CAF/CMSCOMM/COMM_DQM/certification/Collisions17/13TeV/Legacy_2017/Cert_294927-306462_13TeV_UL2017_Collisions17_MuonJSON.txt'
    elif year == '2016':
        lumi_mask = '/afs/cern.ch/cms/CAF/CMSCOMM/COMM_DQM/certification/Collisions16/13TeV/Legacy_2016/Cert_271036-284044_13TeV_Legacy2016_Collisions16_JSON_MuonPhys.txt'


    config.Data.unitsPerJob = units_per_job
    config.Data.lumiMask = lumi_mask
    submit(config)

