import FWCore.ParameterSet.Config as cms
import FWCore.PythonUtilities.LumiList as LumiList

process = cms.Process("Demo")
OUTNAME = 'BFinder'; OUTNAMEr = OUTNAME + '.root'

process.load("FWCore.MessageService.MessageLogger_cfi")
process.MessageLogger.cerr.FwkReport.reportEvery = 100
process.maxEvents = cms.untracked.PSet( input = cms.untracked.int32(16000))

# process.maxEvents = cms.untracked.PSet( input = cms.untracked.int32(13000) )
# process.maxEvents = cms.untracked.PSet( input = cms.untracked.int32(-1) )

process.options = cms.untracked.PSet( wantSummary = cms.untracked.bool(True) )

process.load("Configuration.Geometry.GeometryRecoDB_cff")
process.load('Configuration.StandardSequences.GeometryRecoDB_cff')
process.load('Configuration.StandardSequences.MagneticField_AutoFromDBCurrent_cff')
process.load('Configuration.StandardSequences.EndOfProcess_cff')
process.load('Configuration.StandardSequences.FrontierConditions_GlobalTag_condDBv2_cff')
process.load("TrackingTools/TransientTrack/TransientTrackBuilder_cfi")

## dataset status=* dataset=/ParkingBPH*/Run2018*05May2019*/AOD
process.source = cms.Source("PoolSource", fileNames = cms.untracked.vstring(
    # '/store/data/Run2018C/ParkingBPH2/AOD/05May2019-v1/60002/C32D2F58-7A81-7840-96A5-60F4CE850FB7.root',
    # '/store/data/Run2018D/ParkingBPH2/MINIAOD/05May2019promptD-v1/130000/00A645EF-B599-CE46-84D6-7622C0C250E3.root',
    # '/store/data/Run2018A/ParkingBPH4/MINIAOD/05May2019-v1/50000/6672DE0B-6F79-954E-929A-D460FC0EA63F.root',
    # '/store/data/Run2018A/ParkingBPH4/MINIAOD/05May2019-v1/50001/F4063286-BB59-8D47-8C02-E090BCBF4738.root',
    # '/store/data/Run2018A/ParkingBPH4/MINIAOD/05May2019-v1/50000/88D9AC26-C539-0A4F-997D-54C9352D9121.root',
    # '/store/data/Run2018A/ParkingBPH4/MINIAOD/05May2019-v1/50001/FDF586BC-941D-2D4B-AB7E-D9FDBB136069.root',

    # '/store/data/Run2018A/DoubleMuonLowMass/MINIAOD/17Sep2018-v1/100000/07121AF3-3000-BF4B-B4A4-52205C281621.root',

    # '/store/data/Run2018B/Charmonium/MINIAOD/12Nov2019_UL2018-v1/100000/085A6101-18B0-7E40-AD3B-1CD77CF37B95.root',
    # '/store/data/Run2018B/Charmonium/MINIAOD/12Nov2019_UL2018-v1/100000/801D3E01-5B3D-6540-A806-510D3CD864F9.root',
    '/store/data/Run2018D/Charmonium/MINIAOD/12Nov2019_UL2018-v1/00000/037D20FB-7D34-1340-9BEB-0D27827F7E47.root',
    '/store/data/Run2018D/Charmonium/MINIAOD/12Nov2019_UL2018-v1/00000/1C0EB65A-AEE9-644D-93F5-9773C0298404.root',
))
# process.source.lumisToProcess = LumiList.LumiList(filename = 'python/Cert_294927-306462_13TeV_EOY2017ReReco_Collisions17_JSON_MuonPhys.txt').getVLuminosityBlockRange()
##
### JSON FILE
## https://cms-service-dqm.web.cern.ch/cms-service-dqm/CAF/certification/Collisions17/13TeV/PromptReco/Cert_294927-306462_13TeV_PromptReco_Collisions17_JSON_MuonPhys.txt


from Configuration.AlCa.GlobalTag_condDBv2 import GlobalTag

# process.GlobalTag = GlobalTag(process.GlobalTag, '102X_dataRun2_Sep2018Rereco_v1', '') ## for 2018 ABC RERECO
process.GlobalTag = GlobalTag(process.GlobalTag, '106X_dataRun2_v35', '') ## for 2018 UL
# process.GlobalTag = GlobalTag(process.GlobalTag, '102X_dataRun2_Prompt_v15', '') ## for 2018 D PROMPT RECO

process.load("SimGeneral.HepPDTESSource.pythiapdt_cfi")

# process.triggerSelection = cms.EDFilter("TriggerResultsFilter",
#                                         triggerConditions = cms.vstring(
#                                             'HLT_DoubleMu4_LowMassNonResonantTrk_Displaced_v*',
#                                             'HLT_DoubleMu3_TkMu_DsTau3Mu_v*',
#                                             'HLT_Tau3Mu_Mu7_Mu1_TkMu1_IsoTau15_v*',
#                                             'HLT_DoubleMu3_Trk_Tau3mu_v*',
#                                             'HLT_DoubleMu3_Trk_Tau3mu_NoL1Mass_v*',
#                                             'HLT_Tau3Mu_Mu7_Mu1_TkMu1_IsoTau15_Charge1_v*',
#                                             'HLT_Tau3Mu_Mu7_Mu1_TkMu1_Tau15_Charge1_v*',
#                                             'HLT_Tau3Mu_Mu7_Mu1_TkMu1_Tau15_v*',
#                                             'HLT_Dimuon0_LowMass_L1_TM530_v*',
#                                         ),
#                                         hltResults = cms.InputTag( "TriggerResults", "", "HLT" ),
#                                         l1tResults = cms.InputTag( "" ),
#                                         throw = cms.bool(False)
#                                         )

process.demo = cms.EDAnalyzer('Xb_frame',

                              Muons               = cms.InputTag("slimmedMuons"),
                              Tracks              = cms.InputTag("packedPFCandidates"),
                              VtxSample           = cms.InputTag("offlineSlimmedPrimaryVertices"),
                              beamSpotTag         = cms.InputTag("offlineBeamSpot"),
                              TriggerResults      = cms.InputTag("TriggerResults", "", "HLT"),

                              fileName            = cms.untracked.string(OUTNAMEr),
                              )

process.TFileService = cms.Service("TFileService",
                                   fileName = cms.string(OUTNAMEr)
                                   )


process.mySequence = cms.Sequence(
    # process.triggerSelection *
    process.demo
)

# process.dump=cms.EDAnalyzer('EventContentAnalyzer')
# process.p = cms.Path( process.dump * process.mySequence)
process.p = cms.Path( process.mySequence)
process.schedule = cms.Schedule(process.p)
